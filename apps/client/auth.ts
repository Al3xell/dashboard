import NextAuth, { Account, Profile } from 'next-auth';
import { type TokenSet, UserInfo, Session } from '@auth/core/types';
import { type AdapterUser } from '@auth/core/adapters';
import { type JWT } from '@auth/core/jwt';
import Keycloak from 'next-auth/providers/keycloak';
import dayjs from 'dayjs';
import utc from 'dayjs/plugin/utc';

export const {
    handlers: { GET, POST },
    auth,
    signIn,
    signOut,
} = NextAuth({
    providers: [
        Keycloak({
            clientId: process.env.KEYCLOAK_CLIENT_ID,
            clientSecret: process.env.KEYCLOAK_CLIENT_SECRET,
            issuer: process.env.KEYCLOAK_ISSUER,
        }),
    ],
    secret: process.env.AUTH_SECRET,
    trustHost: true,
    callbacks: {
        // @ts-ignore
        async jwt({ token, account, profile }: { token: JWT, account: Account, profile: Profile }) {
            if (account) {
                // Save the access token and refresh token in the JWT on the initial login
                token.id_token = account.id_token;
                token.access_token = account.access_token;
                token.expires_at = Math.floor(
                    Date.now() / 1000 + account.expires_in
                );
                token.refresh_token = account.refresh_token;
            }
            if (profile) {
                token.user = {
                    firstName: profile.given_name,
                    lastName: profile.family_name,
                    email: profile.email,
                    phone: profile.attributes.phone,
                    roles: profile.roles,
                    birthday: profile.birthday,
                } as AdapterUser & UserInfo;
            }

            if (Date.now() < token.expires_at * 1000) {
                // If the access token has not expired yet, return it
                return token;
            } else {
                // If the access token has expired, try to refresh it
                try {
                    // https://accounts.google.com/.well-known/openid-configuration
                    // We need the `token_endpoint`.
                    const details = {
                        client_id: process.env.KEYCLOAK_CLIENT_ID,
                        client_secret: process.env.KEYCLOAK_CLIENT_SECRET,
                        grant_type: 'refresh_token',
                        refresh_token: token.refresh_token,
                    };
                    const formBody: string[] = [];
                    Object.entries(details).forEach(
                        ([key, value]: [string, any]) => {
                            const encodedKey = encodeURIComponent(key);
                            const encodedValue = encodeURIComponent(value);
                            formBody.push(encodedKey + '=' + encodedValue);
                        }
                    );
                    const formData = formBody.join('&');
                    const url = `${process.env.KEYCLOAK_BASE_URL}/token`;

                    const response = await fetch(url, {
                        headers: {
                            'Content-Type': 'application/x-www-form-urlencoded',
                        },
                        method: 'POST',
                        body: formData,
                    });
                    const tokens: TokenSet = await response.json();

                    if (!response.ok) throw tokens;

                    return {
                        ...token, // Keep the previous token properties
                        access_token: tokens.access_token,
                        expires_at: Math.floor(
                            Date.now() / 1000 + tokens.expires_in
                        ),
                        // Fall back to old refresh token, but note that
                        // many providers may only allow using a refresh token once.
                        refresh_token:
                            tokens.refresh_token ?? token.refresh_token,
                    };
                } catch (error) {
                    console.error('Error refreshing access token', error);
                    // The error property will be used client-side to handle the refresh token error
                    return {
                        ...token,
                        error: 'RefreshAccessTokenError' as const,
                    };
                }
            }
        },
        // @ts-ignore
        async session({ session, token }: { session: Session, token: JWT}) {
            dayjs.extend(utc);
            const userInfo: UserInfo = token.user as UserInfo;
            session.user = {
                firstName: userInfo.firstName,
                lastName: userInfo.lastName,
                email: userInfo.email,
                phone: userInfo.phone,
                roles: userInfo.roles,
                birthday: userInfo.birthday,
            } as AdapterUser & UserInfo;
            session.access_token = token.access_token;
            session.refresh_token = token.refresh_token;
            session.expires_at = dayjs(token.expires_at * 1000).utc(true).format('YYYY-MM-DD HH:mm:ss');
            session.error = token.error;
            return session;
        },
    },
    events: {
      // @ts-ignore
        async signOut({ token }: { token: JWT }): Promise<void> {
            const issuerUrl = process.env.KEYCLOAK_ISSUER;
            const logOutUrl = new URL(
                `${issuerUrl}/protocol/openid-connect/logout`
            );
            logOutUrl.searchParams.set('id_token_hint', token.id_token);
            await fetch(logOutUrl);
        },
    },
}) ;

declare module '@auth/core/types' {
    interface Session {
        user?: UserInfo;
        access_token?: string;
        refresh_token?: string;
        expires_at?: string;
        error?: 'RefreshAccessTokenError';
    }

    interface UserInfo {
        firstName: string;
        lastName: string;
        email: string;
        phone?: string;
        roles: string[];
        birthday?: string;
    }
}

declare module '@auth/core/jwt' {
    interface JWT {
        id_token: string;
        access_token: string;
        expires_at: number;
        refresh_token: string;
        error?: 'RefreshAccessTokenError';
    }
}
