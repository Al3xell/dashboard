import { Button } from '@nextui-org/button'
import { Link } from '@nextui-org/link'
import { Metadata } from 'next'
import Image from 'next/image'

export const metadata: Metadata = {
    title: "Où êtes vous ?",
}

export default function NotFound() {
    return (
        <div className='h-full flex flex-col gap-4 p-4 items-center'>
            <div className='flex gap-4 items-center h-1/2'>
                <Image alt='not-found' src={"/not-found.jpg"} width={300} height={300} className='rounded-xl'  />
                <h1 className='font-bold text-default-600 text-9xl'>Oups...</h1>
            </div>
                <h2 className='font-bold text-default-600 text-2xl'>Vous vous dirigez quelque part, mais nous ne trouvons pas le chemin... 😢</h2>
                <Button as={Link} href="/" className='bg-primary text-background hover:scale-110'>Revenez parmis nous 😄</Button>
        </div>
    )
}