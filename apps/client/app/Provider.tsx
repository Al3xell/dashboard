'use client'

import SignIn from "@/components/auth/SignIn";
import { AntdRegistry } from "@ant-design/nextjs-registry";
import { ConfigProvider, notification } from "antd";
import { Session } from "next-auth";
import { SessionProvider } from "next-auth/react";
import { ReactNode } from "react";
import { ThemeProvider as NextThemesProvider } from "next-themes";
import { SWRConfig } from "swr";
import { InfoData } from "@/types/fetch";
import apiCall from "@/lib/apiCall";
import { NextUIProvider, semanticColors } from "@nextui-org/react";
import { Poppins } from "next/font/google";

const poppins = Poppins({ weight: "400", subsets: ["latin"]})

export default function Providers({ children, session }: { children: ReactNode, session: Session }) {
    notification.config({
        placement: "bottomRight",
        duration: 3
    })
    return (
        <SessionProvider refetchOnWindowFocus={true} session={session}>
            <SWRConfig value={{
                fetcher: (infoData: InfoData) => apiCall(infoData),
                keepPreviousData: true,
            }}>
                <AntdRegistry>
                    <ConfigProvider theme={{ hashed: false }}>
                        <NextUIProvider className="h-full w-full">
                            <NextThemesProvider defaultTheme="light" attribute="class">
                                {children}
                            </NextThemesProvider>
                        </NextUIProvider>
                    </ConfigProvider>
                </AntdRegistry>
            </SWRConfig>
        </SessionProvider>
    );
};
