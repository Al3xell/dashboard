import './globals.css'
import { Poppins,  } from 'next/font/google'
import Provider from './Provider'
import { ReactNode } from 'react'
import { Main } from '@/components/layout/Main'
import { auth } from '@/auth'
import SignIn from '@/components/auth/SignIn'

const poppins = Poppins({ weight: "400", subsets: ["latin"]})

export const metadata = {
  title: {
    default: '... | App',
    template: '%s | App',
  },
}

export default async function RootLayout({ children }: { children: ReactNode }) {
  const session = await auth()
  return (
    <html lang="fr" suppressHydrationWarning className={`${poppins.className}`}>
      <head />
      <body className={`min-h-screen min-w-screen h-screen w-screen`}>
        <Provider session={session}>
          {/* <SignIn /> */}
          <Main>
              {children}
          </Main>
        </Provider>
      </body>
    </html>
  )
}
