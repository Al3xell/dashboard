import { NextResponse } from 'next/server';
import { auth } from './auth';
import { hash } from './lib/hash';
export default auth(async (req) => {
    const host = process.env.AUTH_URL;

    const isSecure = process.env.AUTH_URL?.startsWith('https://') ?? false;

    const cookieName = isSecure
        ? '__Host-authjs.csrf-token'
        : 'authjs.csrf-token';

    let token = await auth();
    // the user is authorized, let the middleware handle the rest
    if (token && token.error == undefined) {
        const headers = new Headers(req.headers);
        headers.set('x-pathname', req.nextUrl.pathname);
        return NextResponse.next({
            request: {
                headers,
            },
        });
    }

    const cookieCsrfToken = req.cookies.get(cookieName)?.value;
    const csrfToken =
        cookieCsrfToken?.split('|')?.[0] ??
        (await fetch(`${process.env.AUTH_URL}/csrf`).then((response) =>
            response.json().then((csrfJson) => csrfJson.csrfToken)
        )) ??
        '';
    const csrfTokenHash =
        cookieCsrfToken?.split('|')?.[1] ??
        (await hash(`${csrfToken}${process.env.AUTH_SECRET}`));
    const cookie = `${csrfToken}|${csrfTokenHash}`;
    const res = await fetch(`${host}/signin/keycloak`, {
        method: 'post',
        headers: {
            'Content-Type': 'application/x-www-form-urlencoded',
            'X-Auth-Return-Redirect': '1',
            cookie: `${cookieName}=${cookie}`,
        },
        credentials: 'include',
        redirect: 'follow',
        body: new URLSearchParams({
            csrfToken,
            callbackUrl: req.nextUrl.pathname,
            json: 'true',
        }),
    });
    const data = (await res.json()) as { url: string };

    return NextResponse.redirect(data.url, {
        headers: {
            'Set-Cookie': res.headers.get('set-cookie') ?? '',
        },
    });
});

// Optionally, don't invoke Middleware on some paths
export const config = {
    matcher: ['/((?!api|_next/static|_next/image|favicon.ico).*)'],
};
