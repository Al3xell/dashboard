"use client"

import { Layout } from "antd";
import { ReactNode } from "react";
import Banner from "./Banner";
import ButtonsHeader from "./ButtonsHeader";
import SiderLayout from "./SiderLayout";

const { Header, Content } = Layout;

export function Main({ children }: { children: ReactNode }) {
    return (
        <main className="h-full flex flex-col">
            <div className="flex bg-primary-400 gap-4 justify-between px-8 py-4 items-center">
                <Banner />
                <ButtonsHeader />
            </div>
            <div className="flex flex-grow">
                <SiderLayout />
                <Content>
                    {children}
                </Content>
            </div>
        </main>
    )
}