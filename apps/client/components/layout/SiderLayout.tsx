"use client"

import { MenuProps } from "antd";
import { useBoolean, useToggle } from "ahooks";
import { HomeFilled } from "@ant-design/icons";
import { Button } from "@nextui-org/button";
import Chevron from "../icons/utils/Chevron";
import { semanticColors } from "@nextui-org/react";
import ItemSider from "../ui/ItemSider";
import Home from "../icons/utils/Home";

const items: MenuProps['items'] = [
    {
        key: 'dashboard',
        icon: (
            <HomeFilled />
        ),
        label: 'Tableau de bord',

    },
    
]

export default function SiderLayout() {
    const [isCollapsed, {toggle: toogleIsCollapsed}] = useBoolean()
    const [width, { toggle: toggleWidth }] = useToggle('w-52', 'w-16')
    return (
        <div className={`h-full flex flex-col gap-4 bg-primary-400 ${width} transition-all`}>
            <div className="flex-grow">
                <ItemSider tooltipProps={{ placement: "right", offset: 5, isDisabled: !isCollapsed }} buttonProps={{ className: "min-w-0", isIconOnly: isCollapsed }} label="Tableau de bord">
                    <Home className="w-8 h-8" />
                </ItemSider>
            </div>
            <Button isIconOnly className="w-full rounded-none bg-transparent self-end" onPress={() => {
                toogleIsCollapsed()
                toggleWidth()
            }}>
                <Chevron fill={semanticColors.light.default[50]} className={`transition-all w-8 h-8 ${isCollapsed ? "rotate-180" : ""}`} />
            </Button>
        </div>
    )
}