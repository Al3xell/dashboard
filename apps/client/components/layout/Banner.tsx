import { Skeleton } from "@nextui-org/skeleton"
import { Image } from "@nextui-org/image"

export default function Banner() {
    return (
        <div className="flex items-center gap-4 w-60 h-10">
            <Image src="/favicon.ico" radius="none" alt="Dashboard-logo" />
            <Skeleton className="flex-grow rounded-lg">
                <div className="w-full h-8 bg-default"></div>
            </Skeleton>
        </div>
    )
}