import Bell from "../icons/utils/Bell"
import Account from "../icons/utils/Account"
import { semanticColors } from "@nextui-org/theme"
import IconeButton from "../ui/IconeButtons"


export default function ButtonsHeader() {
    return (
        <div className="flex gap-8 items-center ">
            <IconeButton label="Mon compte">
                <Account fill={semanticColors.light.default[50]} className="h-8 w-8" />
            </IconeButton>
            <IconeButton label="Notifications">
                <Bell fill={semanticColors.light.warning[500]} className="h-8 w-8" />
            </IconeButton>
        </div>
    )
}