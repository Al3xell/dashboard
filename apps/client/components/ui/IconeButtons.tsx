import { Button, ButtonProps } from "@nextui-org/button"
import { Tooltip, TooltipProps } from "@nextui-org/tooltip"
import { ReactNode } from "react"

export default function IconeButton({ children, label, buttonProps, tooltipProps }: { children: ReactNode, label: string, buttonProps?: ButtonProps, tooltipProps?: TooltipProps}) {
    return (
        <Tooltip content={label} color="primary" offset={tooltipProps?.offset || 20} placement={tooltipProps?.placement}>
            <Button onPress={buttonProps?.onPress} isIconOnly aria-label={label} className={`bg-transparent rounded-none w-8 h-8 hover:scale-125 ${buttonProps?.className}`}>
                {children}
            </Button>
        </Tooltip>
    )
}