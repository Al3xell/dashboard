import { Button, ButtonProps } from "@nextui-org/button"
import { Tooltip, TooltipProps } from "@nextui-org/tooltip"
import { ReactNode } from "react"

export default function ItemSider({ children, label, buttonProps, tooltipProps }: { children: ReactNode, label: string, buttonProps?: ButtonProps, tooltipProps?: TooltipProps}) {
   
    return (
        <Tooltip content={label} isDisabled={tooltipProps?.isDisabled} color="primary" offset={tooltipProps?.offset || 20} placement={tooltipProps?.placement}>
            <Button onPress={buttonProps?.onPress} isIconOnly={buttonProps?.isIconOnly} aria-label={label} className={`bg-transparent rounded-none w-full h-auto px-2 ${buttonProps?.className}`}>
                <div className={`bg-primary-200 flex ${buttonProps?.isIconOnly ? "gap-0" : "gap-4"} items-center p-2 rounded-xl w-full transition-all`}>
                    {children}
                    <div className={`transition-all overflow-hidden ${buttonProps?.isIconOnly ? "w-0" : "w-full"}`}>{label}</div>
                </div>
            </Button>
        </Tooltip>
    )
}