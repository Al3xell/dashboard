"use server"

import { InfoData } from "@/types/fetch";
import { UnauthorizedError } from "./UnauthorizedError";
import { UnhandledError } from "./UnhandledError";
import { auth } from "@/auth";

export default async function apiCall(infoData: InfoData) {
    const session = await auth()
    const url = `${process.env.NEXT_PUBLIC_API_URL}/${infoData.url}`
    return await fetch(url, {
        method: infoData.method,
        body: infoData.multipart ? infoData.data : JSON.stringify(infoData.data),
        headers: infoData.multipart
            ? {
                Authorization: "Bearer " + session?.access_token,
                userInfo: JSON.stringify(session?.user),
            }
            : {
                Authorization: "Bearer " + session?.access_token,
                'Content-type': 'application/json',
                userInfo: JSON.stringify(session?.user),
            },
    }).then(async (response) => {
        if (response.status === 200 || response.status === 201) return await response.json();
        if (response.status === 403) throw new UnauthorizedError();
        else throw new UnhandledError()
    });
} 