export interface InfoData {
    method: string;
    url: string;
    data?: any;
    multipart?: boolean;
}