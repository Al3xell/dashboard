

declare module '@auth/core/types' {
    interface Session {
        user?: UserInfo;
        access_token?: string;
        refresh_token?: string;
        expires_at?: string;
        error?: 'RefreshAccessTokenError';
    }

    interface UserInfo {
        firstName: string;
        lastName: string;
        email: string;
        phone?: string;
        roles: string[];
        birthday?: string;
    }

    interface TokenSet {
        access_token: string;
        refresh_token: string;
        expires_in: number;
    }
}

declare module '@auth/core/jwt' {
    interface JWT {
        id_token: string;
        access_token: string;
        expires_at: number;
        refresh_token: string;
        user?: UserInfo;
        error?: 'RefreshAccessTokenError';
    }
}
