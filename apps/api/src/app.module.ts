import { Module } from '@nestjs/common';
import { ConfigModule } from '@nestjs/config';
import { APP_GUARD } from '@nestjs/core';
import {
    AuthGuard,
    KeycloakConnectModule,
    PolicyEnforcementMode,
    ResourceGuard,
    RoleGuard,
    TokenValidation,
} from 'nest-keycloak-connect';
import { SequelizeModule } from '@nestjs/sequelize';
import { DBConfig } from './core/databases/DB.config';
import { UserModule } from './utils/user/user.module';

@Module({
    imports: [
        ConfigModule.forRoot({
            envFilePath: '.env',
            isGlobal: true,
            expandVariables: true,
            cache: true,
        }),
        KeycloakConnectModule.register({
            authServerUrl: process.env.KEYCLOAK_AUTH_URL,
            realm: process.env.KEYCLOAK_REALM,
            clientId: process.env.KEYCLOAK_CLIENT_ID,
            secret: process.env.KEYCLOAK_CLIENT_SECRET,
            policyEnforcement: PolicyEnforcementMode.PERMISSIVE, // optional
            tokenValidation: TokenValidation.ONLINE, // optional
            // Secret key of the client taken from keycloak server
        }),
        SequelizeModule.forRoot({
            ...DBConfig,
            autoLoadModels: true,
            synchronize: process.env.NODE_ENV != 'production',
            retryAttempts: 3,
        }),
        UserModule,
    ],
    providers: [
        {
            provide: APP_GUARD,
            useClass: AuthGuard,
        },
        {
            provide: APP_GUARD,
            useClass: ResourceGuard,
        },
        {
            provide: APP_GUARD,
            useClass: RoleGuard,
        },
    ],
})
export class AppModule {}
